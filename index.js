const request = require('request');
const fs = require('fs');
const path = require('path');
const config = require('./config');
const xpath = require('xpath');
const DOMParser = require('xmldom').DOMParser;
const progress = require('progress-stream');

const winston = require('winston');
const LOGGER = new (winston.Logger)({
    transports: [new (winston.transports.Console)({level: process.env.LOG_LEVEL || 'info', colorize: true})],
});

/**
 *
 * @param path
 * @param image {url: '', name: ''}
 * @returns {Promise<string>}
 */
let saveImage = async (destinationFolder, image) => {
    return new Promise((resolve, reject) => {
        LOGGER.debug('Starting download image url %s', image.url);
        LOGGER.debug('Path %s', path.resolve(destinationFolder, image.name));
        const progressStream = progress({time: 100}, progress => {
            process.stdout.write(`[${Math.round(progress.percentage)} % - ${Math.round(progress.speed/1024)} KB/s]Downloading ${image.name}\r`);
        });
        request
            .get(image.url, (error, response, body) => {
                if (error) {
                    LOGGER.error(error);
                    return reject(error);
                }
            })
            .on('response', response => {
                const fileLengthInBytes = response.headers['content-length'];
                progressStream.setLength(fileLengthInBytes);
                image.size = response.headers['content-length'];
                LOGGER.debug('Response OK');
            })
            .pipe(progressStream)
            .pipe(
                fs.createWriteStream(path.resolve(destinationFolder, image.name))
                    .on('finish', () => {
                        LOGGER.debug('Write success', image.name);
                        resolve(image.name);
                    })
                    .on('error', () => {
                        LOGGER.error('Write error', image.name);
                        reject(image.name);
                    })
            )
    });
};

/***
 * get images from page by url page
 * @param pageUrl the page contain images
 * @return {Promise<Image[]>}
 */
let scanImages = (pageUrl) => {
    LOGGER.debug('Scan images from %s', pageUrl);
    return new Promise((resolve, reject) => {
        request.get(pageUrl, (error, response, body) => {
            if (error) {
                LOGGER.error(error);
                return reject(error)
            }
            LOGGER.debug('Response OK');

            LOGGER.debug('Parsing DOM to get images.');
            const images = parseImages(body);
            LOGGER.debug('Parsed Images', images);

            resolve(images);
        });
    });

};
/***
 * parse body to get url images
 * @param body str
 * @return {Image[]}
 */
let parseImages = (body) => {
    const doc = new DOMParser().parseFromString(body);
    const nodes = xpath.select('//div[@id="posts"]//img[@src]/@src', doc);

    const imageUrls = nodes.map(node => {
        const url = node.value.replace(/(-\d+x\d+)(\.)/gi, '$2');
        return {
            url: url,
            name: url.replace(/^.*\//gi, '')
        }
    });
    return imageUrls;
};

const pageUrl = 'http://animenylon.pl/category/all/hentai-ecchi/page/154/';

const downloadAllImagesInPage = async (pageUrl) => {
    let images = [];
    try {
        images = await promiseTimeout(config.timeout_download_content_page_in_millis, scanImages(pageUrl))
    } catch (ex) {
        throw new Error('Can not scan page or timeout ' + pageUrl);
    }

    for (let i = 0; i < images.length; i++) {
        const startTime = new Date();
        const image = images[i];
        LOGGER.debug('Start %s', image.url);
        try {
            await promiseTimeout(config.timeout_download_image_in_millis, saveImage(config.save_folder, image))
            LOGGER.info('Downloaded %s in [%s ms - %s KB]', image.name, new Date() - startTime, Math.round(image.size/1024));
        } catch (ex) {
            LOGGER.warn('TIMEOUT - Download image %s TIMEOUT. Skip!', image.name);
        }

    }
};
const promiseTimeout = function (ms, promise) {

    // Create a promise that rejects in <ms> milliseconds
    let timeout = new Promise((resolve, reject) => {
        let id = setTimeout(() => {
            clearTimeout(id);
            reject('Timed out in ' + ms + 'ms.')
        }, ms)
    });

    // Returns a race between our timeout and the passed in promise
    return Promise.race([
        promise,
        timeout
    ])
};

const run = async () => {
    let failsCount = 0;

    let download = async (currentPage) => {
        LOGGER.warn(currentPage);
        if (failsCount < config.fails_count_before_stop &&
            (!config.options.end_page || currentPage <= config.options.end_page)) {
            const startTime = new Date();
            const url = config.page_pattern.replace('%PAGE_NUMBER%', currentPage);
            try {
                await downloadAllImagesInPage(url);
            } catch (ex) {
                LOGGER.error(ex);
                failsCount++;
            }

            LOGGER.info('DOWNLOADED all images from %s in [%s ms]', url, new Date() - startTime);

            return await download(++currentPage);
        }


    };

    await download(config.options.start_page || 1);
};

run();
