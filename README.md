## Animenylon Crawler
Crawl and download images from animenylon.pl

### Usage

Before you run development or production, configure `config.js`:
```javascript
{
    options: {
        start_page: 26,
        end_page: 0 // 0 for unlimited (stop when fails n/a times in config)
    },
    save_folder: path.resolve(__dirname, 'downloaded/'), // should be absolute path
    fails_count_before_stop: 5,
    page_pattern: 'http://animenylon.pl/category/all/hentai-ecchi/page/%PAGE_NUMBER%/',
    timeout_download_image_in_millis: 15000,
    timeout_download_content_page_in_millis: 10000
}
```

#### Development
`npm run dev`

#### Production
//todo